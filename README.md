# Trendanalyse der Energiewirtschaft

## Crawler:
To run Crawler: $ scrapy crawl <crawler_name>

Crawlers:
- ud_spider:
    crawls the overview pages and returns the articles from the first 10 pages, their hrefs and metadata
- extract_text_spider:
    takes the output-csv-file from the ud_spider and scrapes the text  and meta-data from all articles in this csv-file

## Dash App
To run Dash App: $ python app.py
App runs on server http://127.0.0.1:8050/ in your web browser.

## Jupyter Notebooks
Notebooks are for preprocessing and experimentation
Some Notebooks import use functions from other noteboooks.

## Project Overview:
├── README.md

├── dash

│   ├── all_keywords.py

│   ├── app.py

│   ├── assets

│   │   ├── eon.png

│   │   ├── eon_logo.jpg

│   │   └── stylesheet.css

│   ├── metadatagraphs.py

│   └── single_keywords.py
├── data

│   ├── analysis

│   ├── plots

│   ├── preprocessed

│   └── raw

├── energytrends

│   ├── energytrends

│   │   ├── __init__.py

│   │   ├── __pycache__

│   │   │   ├── __init__.cpython-38.pyc

│   │   │   └── settings.cpython-38.pyc

│   │   ├── items.py

│   │   ├── middlewares.py

│   │   ├── pipelines.py

│   │   ├── settings.py

│   │   └── spiders
│   │       ├── Test_Spider.py

│   │       ├── __init__.py

│   │       ├── __pycache__

│   │       │   ├── Test_Spider.cpython-38.pyc

│   │       │   ├── __init__.cpython-38.pyc

│   │       │   ├── extract_text_spider.cpython-38.pyc

│   │       │   └── ud_spider.cpython-38.pyc

│   │       ├── extract_text_spider.py

│   │       └── ud_spider.py

│   ├── news1.csv

│   ├── news2.csv

│   ├── posts.json

│   └── scrapy.cfg

├── notebooks
│   ├── CrawlingPlayground.ipynb

│   ├── DoubleWords.ipynb

│   ├── GeneralVisualization.ipynb

│   ├── Keyword_Graph.ipynb

│   ├── KeywordsAll.ipynb

│   ├── Plotly_Visualisierung.ipynb

│   ├── Preprocessing_UD.ipynb

│   ├── SingleKeywords.ipynb

│   └── Textanalysis.ipynb

└── oldapp.txt



