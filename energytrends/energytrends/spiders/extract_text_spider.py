from abc import ABC

import scrapy
import pandas as pd
from datetime import datetime


class extract_text_spider(scrapy.Spider):
    counter = 0
    domain = 'https://www.utilitydive.com'
    name = "extract_text_spider"
    article_sum = pd.read_csv('../data/raw/11_12_News_UtilityDive.csv')
    article_text = pd.DataFrame(
        columns={'Fulltext', 'URL', 'Title', 'DiveBrief', 'Topic', 'Author', 'Teasertext', 'Date'})
    start = article_sum.at[counter, 'URL']
    start_urls = (
        start,
    )

    def parse(self, response):
        # Get Text-Body of Article
        body = response.xpath('//*[@id="main-content"]/article/div[2]/div[2]/div/div[1]')

        # Trendline articles cannot be scraped!!!
        # Extract the text
        text = body.css('p *::text').getall()

        if len(text) < 5:
            body = response.css('div.article-body')
            text = body.css('p *::text').getall()

        # Extract the dive brief and clean '\n’ directly out
        brief = list(filter(lambda x: '\n' not in x, body.xpath('//li/text()').getall()))
        print('Outside brief:' + str(brief))

        # crawl different briefing types if available
        if not brief or len(brief) < 6:
            brief = list(filter(lambda x: '\n' not in x, body.xpath('//li/p/span/span/text()').getall()))
            print('In if: ' + str(brief))
            if not brief:
                brief = 'No Dive Brief'

        # Catch different Dive Brief Structures
        elif brief[0] == '—\u200b' or brief[0] == '\u200b' or brief[0] == '—' or brief[0] == '\xa0':
            brief = list(filter(lambda x: '\n' not in x, body.xpath('//li/span/span/text()').getall()))
            if not brief:
                brief = 'No Dive Brief'

        # Save data
        self.article_text.loc[self.counter, 'Fulltext'] = text
        self.article_text.loc[self.counter, 'URL'] = str(response.url)
        self.article_text.loc[self.counter, 'Title'] = self.article_sum.loc[self.counter, 'Title']
        self.article_text.loc[self.counter, 'Topic'] = self.article_sum.loc[self.counter, 'Topic']
        self.article_text.loc[self.counter, 'Teasertext'] = self.article_sum.loc[self.counter, 'Feed_Description']
        self.article_text.loc[self.counter, 'Author'] = self.article_sum.loc[self.counter, 'Author']
        self.article_text.loc[self.counter, 'Date'] = self.article_sum.loc[self.counter, 'Date']
        self.article_text.loc[self.counter, 'DiveBrief'] = brief

        # Get next URL
        next_url = self.article_sum.at[self.counter + 1, 'URL']

        # Loop over all URLs
        if next_url is not None:
            self.counter = self.counter + 1
            print('Request new: ' + str(self.article_sum.at[self.counter, 'URL']))

            # Skip not found articles
            if next_url == 'https://www.utilitydive.comNone':
                self.article_text.loc[self.counter, 'URL'] = str(response.url)
                self.article_text.loc[self.counter, 'Title'] = self.article_sum.loc[self.counter, 'Title']
                self.article_text.loc[self.counter, 'Fulltext'] = 'NotFound'
                self.counter = self.counter + 1

        # Request new URL
        try:
            print("Current Line: " + str(self.counter))
            yield scrapy.Request(self.article_sum.at[self.counter, 'URL'], callback=self.parse, dont_filter=True)
        except KeyError:
            print("Crawling finished with Request: " + str(self.counter))

        file_name = 'Texts_UtilityDive'
        now = datetime.now()

        output = self.article_text
        yield output.to_csv('../data/raw/' + now.strftime("%m_%d") + str(file_name) + '.csv', sep=",")
