#
# Spyder for https://www.utilitydive.com
# @author Florian Gräbe
#

import scrapy
import pandas as pd
from datetime import datetime


class ud_spider(scrapy.Spider):
    count = 0
    domain = 'https://www.utilitydive.com'
    name = "ud_spider"
    start_urls = [
        'https://www.utilitydive.com/topic/Generation/?page=1',
        'https://www.utilitydive.com/topic/transmission-distribution/?page=1',
        'https://www.utilitydive.com/topic/solar/?page=1',
        'https://www.utilitydive.com/topic/storage/?page=1',
        'https://www.utilitydive.com/topic/demand-response/?page=1',
        'https://www.utilitydive.com/topic/distributed-energy/?page=1',
        'https://www.utilitydive.com/topic/policy/?page=1',
        'https://www.utilitydive.com/topic/technology/?page=1',
        'https://www.utilitydive.com/topic/market-news/?page=1',
    ]

    news_articles = pd.DataFrame(columns={'Title', 'Author', 'Feed_Description', 'Date', 'URL', 'Topic'})

    def parse(self, response):

        counter = self.count
        # Get news feed items one by one
        for news in response.css('li.row.feed__item'):
            try:
                self.news_articles.at[counter, 'Title'] = news.css('.medium-8.columns h3 a::text').get()
                self.news_articles.at[counter, 'Feed_Description'] = news.css('.medium-8.columns p::text').get()
                self.news_articles.at[counter, 'Date'] = news.css('.medium-8.columns span::text')[-1].get()
                self.news_articles.at[counter, 'URL'] = str(self.domain) + str(
                    news.css('.medium-8.columns h3 a::attr(href)').get())
                topic_name = str(response.request.url).rsplit('/', 2)[-2]
                self.news_articles.at[counter, 'Topic'] = topic_name
            # Catch errors for missing dates in Sponsered Articles
            except IndexError:
                self.news_articles.at[counter, 'Title'] = news.css('.medium-8.columns h3 a::text').get()
                self.news_articles.at[counter, 'Feed_Description'] = news.css('.medium-8.columns p::text').get()
                self.news_articles.at[counter, 'URL'] = str(self.domain) + str(
                    news.css('.medium-8.columns h3 a::attr(href)').get())
                topic_name = str(response.request.url).rsplit('/', 2)[-2]
                self.news_articles.at[counter, 'Topic'] = topic_name
                self.news_articles.at[counter, 'Date'] = "NaN"

            # catch errors due to author -> because authors are sometimes linked and sometimes not
            try:
                if news.css('.medium-8.columns a::text')[1].get() is not None:
                    self.news_articles.at[counter, 'Author'] = news.css('.medium-8.columns a::text')[1].get()
                else:
                    self.news_articles.at[counter, 'Author'] = 'NotFound'

            except IndexError:
                try:
                    self.news_articles.at[counter, 'Author'] = news.css('.medium-8.columns span::text')[0].get()
                except IndexError:
                    self.news_articles.at[counter, 'Author'] = 'NotFound'

            # Scrape Trendline articles that have a different shape

            if self.news_articles.at[counter, 'Title'] == "" or self.news_articles.at[counter, 'Title'] is None:
                try:
                    self.news_articles.at[counter, 'Title'] = news.css('.medium-6.columns h3 a::text').get()
                    self.news_articles.at[counter, 'Feed_Description'] = news.css('.medium-6.columns p::text').get()
                    self.news_articles.at[counter, 'Date'] = 'NaN'
                    self.news_articles.at[counter, 'URL'] = str(self.domain) + str(news.css('.medium-6.columns h3 '
                                                                                            'a::attr(href)').get())
                    topic_name = str(response.request.url).rsplit('/', 2)[-2]
                    self.news_articles.at[counter, 'Topic'] = topic_name
                    self.news_articles.at[counter, 'Author'] = news.css('.secondary-label ::text').get()

                except IndexError:
                    print('IndexError for Trendline')

            counter = counter + 1

        # prepare for next iteration
        self.count = counter

        # get info from from next pages

        next_no = int(response.request.url[-1]) + 1
        next_page = '?page=' + str(next_no)


        # crawl first 10 pages and exclude going back to page 1 after 10 Page
        if next_no is not 1 and int(response.request.url[-1]) < 11:
            print('Next_Page:' + str(next_page))
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)

        file_name = '_News_UtilityDive.csv'
        now = datetime.now()
        yield self.news_articles.to_csv('../data/raw/' + now.strftime("%m_%d") + str(file_name), sep=",")
