import scrapy


class TestSpider(scrapy.Spider):
    name = "TestSpider"
    start_urls = [
        'https://blog.scrapinghub.com/'
    ]

    # https://www.utilitydive.com/topic/solar/
    # https://www.utilitydive.com/news/
    # 'https://www.utilitydive.com/news/geothermal-energy-is-surging-battered-oil-and-gas-companies-should-take-a/582377/'

    def parse(self, response):
        for post in response.css('div.post-item'):
            yield {
                'title': post.css('.post-header h2 a::text')[0].get(),
                'date': post.css('.post-header a::text')[1].get(),
                'author': post.css('.post-header a::text')[2].get()
            }
        next_page = response.css('a.next-posts-link::attr(href)').get()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)


