import plotly.graph_objs as go
import pandas as pd
import networkx as nx
import numpy as np
import matplotlib.pyplot as plt
from all_keywords import sort_keyword_frame, sum_columns, drop_empty_cols

pd.options.mode.chained_assignment = None  # default='warn' suppress copy warning


def find_articles(data, keyword, category, date=0, select='Keyword'):
    all_keywords = data.all_keywords.copy()
    urls = data.urls.copy()
    dates = data.dates.copy()
    cat = data.categories.copy()
    all_keywords = all_keywords.merge(urls, how='left', on='Title')
    all_keywords = all_keywords.merge(dates, how='left', on='Title')
    all_keywords = all_keywords.merge(cat, how='left', on='Title')

    # Build Dataframe for Table output
    key_df = all_keywords[all_keywords[keyword] > 0]
    if category != 'all':
        key_df = key_df[key_df['Topic'] == category]

    key_df = key_df[['Title', 'URL', 'Date', 'Topic', keyword]]
    if select == 'Date':
        if date == 0:
            # TODO: Sorting by Date doesnt work correctly! Sorts months alphabetically
            key_df.sort_values(by='Date', ascending=True, inplace=True)
        else:
            key_df.sort_values(by='Date', ascending=False, inplace=True)
    else:
        # select== 'Keyword
        key_df.sort_values(by=keyword, ascending=False, inplace=True)

    return key_df


def get_single_keyword_bar(data, keyword='clean energy'):
    all_keywords = data.all_keywords.copy()
    # TODO replace this static line

    # Filter DF
    cl_en = all_keywords[all_keywords[keyword] > 0]

    # Get Keyword Sums
    en_df = pd.DataFrame()
    for column in cl_en.columns:
        try:
            en_df.at[0, column] = cl_en[column].sum()
        except BaseException:
            # Pass to avoid Exception thrown due to the 'Date' column not beeing the right format
            pass
    for colum in en_df.columns:
        if en_df.at[0, colum] == 0:
            en_df = en_df.drop(columns={colum})

    en_df.drop(columns={'FullTextLemma', 'Title'}, inplace=True)
    en_df = sort_keyword_frame(en_df)

    # plot graph
    s_data = [go.Bar(
        x=en_df.columns,
        y=en_df.loc[0])]
    s_keywords = dict(
        title='Keywords that occur with "' + str(keyword) + '" - sorted'
    )

    return dict(data=s_data, layout=s_keywords)


def draw_single_network(data, keyword='clean energy', threshold=10):
    df = data.all_keywords.copy()
    jaccard = data.jaccard.copy()
    keyword_df = df[df[keyword] > 0]

    try:
        keyword_df.drop(columns={'FullTextLemma', 'Title', 'Date', 'URL', 'Topic'}, inplace=True)
    except KeyError:
        # Pass KeyError
        pass

    # drop empty columns
    keyword_df.drop(columns={'FullTextLemma', 'Title'}, inplace=True)
    keyword_df = sum_columns(pd.DataFrame(keyword_df))
    keyword_df = drop_empty_cols(pd.DataFrame(keyword_df))

    # Remove keywords with less than #threshold occurances for better visbility

    for colum in keyword_df:
        try:
            if keyword_df.at[0, colum] < threshold:
                keyword_df.drop(columns={colum}, inplace=True)
        except TypeError:
            print('TypeError in draw_single_network')
            print(colum)

    # Draw Graph
    G = nx.Graph()
    sizes = []
    for col in keyword_df.columns:
        G.add_node(col, count=keyword_df.at[0, col])
        G.add_edge(keyword, col, weight=jaccard.at[keyword, col])
        sizes.append(keyword_df.at[0, col] * 0.02)
    try:
        edges, weights = zip(*nx.get_edge_attributes(G, 'weight').items())

        options = {
            "edge_color": weights,
            "width": 4,
            "edge_cmap": plt.cm.Blues,
            "with_labels": True,
        }

        nx.draw(G, node_size=sizes, **options)
        pos_ = nx.spring_layout(G, k=0.4, scale=20)

    except ValueError:
        print('In Value Error - Single Keywords')
        lay_out = go.Layout(title="ERROR - The selected Keyword has not enough connections to be plotted!")
        err = go.Figure(layout=lay_out)
        return err

    # For each edge, make an edge_trace, append to list
    edge_trace = []
    for edge in G.edges():

        if G.edges()[edge]['weight'] > 0:
            char_1 = edge[0]
            char_2 = edge[1]
            x0, y0 = pos_[char_1]
            x1, y1 = pos_[char_2]

            text = char_1 + '--' + char_2 + ': ' + str(G.edges()[edge]['weight'])

            trace = make_edge([x0, x1, None], [y0, y1, None], text,
                              width=1.8 * G.edges()[edge]['weight'])
            edge_trace.append(trace)

    # Make a node trace
    node_trace = go.Scatter(x=[],
                            y=[],
                            text=[],
                            textposition="top center",
                            mode='markers+text',
                            marker=dict(color='LightSkyBlue',
                                        size=sizes,
                                        line=dict(
                                            color='MediumBlue',
                                            width=weights
                                        )))
    # For each node in G, get the position and size and add to the node_trace
    for node in G.nodes():
        x, y = pos_[node]
        node_trace['x'] += tuple([x])
        node_trace['y'] += tuple([y])
        node_trace['text'] += tuple(['<b>' + node + '</b>'])
    # Customize layout
    layout = go.Layout(
        paper_bgcolor='rgba(0,0,0,0)',  # transparent background
        plot_bgcolor='rgba(0,0,0,0)',  # transparent 2nd background
        xaxis={'showgrid': False, 'zeroline': False},  # no gridlines
        yaxis={'showgrid': False, 'zeroline': False},  # no gridlines
    )
    # Create figure
    fig = go.Figure(layout=layout)
    # Add all edge traces
    for trace in edge_trace:
        fig.add_trace(trace)
    # Add node trace
    fig.add_trace(node_trace)
    # Remove legend
    fig.update_layout(showlegend=False)
    # Remove tick labels
    fig.update_xaxes(showticklabels=False)
    fig.update_yaxes(showticklabels=False)

    return fig


def make_edge(x, y, text, width):
    return go.Scatter(x=x,
                      y=y,
                      # marker_colorscale=plotly.colors.sequential.Blues,
                      line=dict(width=width,
                                color='cornflowerblue',
                                ),
                      hoverinfo='text',
                      text=([text]),
                      mode='lines')


def get_keyword_over_time(data, keyword):
    # Copy Df
    date_df = data.dates.copy()

    all_keys = data.all_keywords.copy()
    # Format DF - extract only relevant articles and sort by data
    date_df['Date'] = pd.to_datetime(date_df['Date'], format='%b %d %Y')
    all_keys = all_keys.merge(date_df, how='left', on='Title')
    all_keys = all_keys[all_keys[str(keyword)] > 0]
    all_keys = all_keys.sort_values(by='Date')

    date_df = pd.DataFrame(columns={'Date', str(keyword)})

    # build full timeline for keyword
    date_df['Date'] = pd.date_range(all_keys['Date'].min(), all_keys['Date'].max())
    # Set correct indices for following loop
    date_df.set_index('Date', inplace=True, drop=False)
    # key_df.set_index('Date', inplace=True, drop=False)
    key_df = all_keys.groupby('Date')[str(keyword)].sum()
    key_df = pd.DataFrame(key_df)
    date_df.fillna(0, inplace=True)
    # loop over dates to build correct dataframe for display

    for d in date_df.index:
        if d in key_df.index:
            date_df.at[d, str(keyword)] = key_df.at[d, str(keyword)]
        else:
            date_df.at[d, str(keyword)] = 0

    # drop NaTs
    date_df = date_df[~pd.isnull(date_df['Date'])]
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=date_df['Date'],
        y=date_df[keyword],
        name="Keyword '" + str(keyword) + "' over time",
        connectgaps=True
    ))
    fig.update_layout(
        title="Keyword '" + str(keyword) + "' over time",
    )
    return fig


def get_correlated_keywords(data, keyword):
    korr = data.korr.copy()
    corr_keywords = ''
    corr_df = get_max_correlated_keywords(korr, keyword)
    counter = 0
    corr_df.fillna(np.NaN)
    if corr_df[counter].round(4) != np.NaN and corr_df[counter].round(4) > 0:
        for keyw in corr_df.index:
            corr_keywords = corr_keywords + str(keyw) + ': ' + str(corr_df[counter].round(4)) + ' ' + "\n"
            counter = counter + 1
    else:
        corr_keywords = "Keyword doesn't occur often enough to calculate correlation matrix"

    return corr_keywords


def get_max_correlated_keywords(korr_matrix, keyword):
    corr_max = korr_matrix.nlargest(5, [str(keyword)])
    return corr_max[str(keyword)]
