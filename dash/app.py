# -*- coding: utf-8 -*-
# Run this app with `python app.py` and
# visit http://127.0.0.1:8050/ in your web browser.

import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_bootstrap_components as dbc
import numpy as np
import dash_table
from DataManagement import DataManagement

# Import Scripts
from metadatagraphs import get_moving_avg_date_graph, get_category_pie, get_author_bar, get_keyword_list
from all_keywords import get_all_keywords_bargraph, draw_total_network
from single_keywords import find_articles, get_single_keyword_bar, draw_single_network, get_keyword_over_time, \
    get_correlated_keywords

# Get data by building data object and then distributing -> reduce the number of I/O Operations
data = DataManagement()
all_keywords = data.all_keywords
cat = data.categories
key_df = find_articles(data, 'clean energy', 'all')

# initialize App
# external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=[dbc.themes.CERULEAN])
# app = dash.Dash(__name__)

# load graphs
all_key_bar = get_all_keywords_bargraph(data, 0)
moving_dates = get_moving_avg_date_graph(data)
category_chart = get_category_pie(data)
author_bar = get_author_bar(data)
net = draw_total_network(data)
single_keyword_bar = get_single_keyword_bar(data, 'clean energy')
single_network = draw_single_network(data, 'clean energy')
single_key_time = get_keyword_over_time(data, 'clean energy')

# Build Custom Components
load_button = dbc.Row(
    [
        dbc.Col([
            dbc.Button("LOAD NEW DATA", id='load-data-button', color="primary", className="ml-2",
                       style={'font-size': '12px'}),
            dbc.Modal(
                [
                    dbc.ModalHeader("LOAD NEW DATASET"),
                    dbc.ModalBody([
                        html.Label("Insert file path to the dataset you want to display: "),
                        dcc.Input(id='data-input', placeholder=' Insert path to data files here...', type='text',
                                  style={'width': '100%'}, debounce=True)
                    ]
                    ),
                    dbc.ModalFooter([
                        dbc.Button("Close", id="close", className="ml-2", style={'align': 'center'}),
                        dbc.Button("Load Data", id="load", className="ml-2", style={'align': 'center'}),
                    ]),
                ],
                id="modal",
                size="xl"
            ),
        ], width="auto",
        ),

    ],
    no_gutters=True,
    className="ml-auto flex-nowrap mt-3 mt-md-0",
    align="center",
)

navbar = dbc.Navbar(
    [
        html.A(
            # Use row and col to control vertical alignment of logo / brand
            dbc.Row(
                [
                    dbc.Col(html.Img(src="/assets/eon_logo.jpg", height="30px")),
                    dbc.Col(dbc.NavbarBrand("Trends in Energy Markets ", style={'font-size': '20px'})),
                ],
                align="center",
                no_gutters=True,
            ),
            href="https://www.eon.com/de/neue-energie/neue-energiewelt/sieben-mal-trend.html",
        ),
        dbc.NavbarToggler(id="navbar-toggler"),
        dbc.Collapse(load_button, id="navbar-collapse", navbar=True),
    ],
    color="dark",
    dark=True,
)

# Define App Layout
app.layout = html.Div(children=[
    # First Block: Title
    dbc.Row(
        [dbc.Col(
            navbar
        )]),

    # Second Block - All KeywordSection

    dbc.CardDeck([
        dbc.Card([
            dbc.CardHeader('All Keywords Analysis'),
            dbc.CardBody([
                dcc.Dropdown(
                    id='allKeywordsDD',
                    options=[
                        {'label': 'All', 'value': 0},
                        {'label': '>100', 'value': 1},
                        {'label': '<100', 'value': 2}
                    ],
                    value=0
                ),
                html.Div(id='out-container'),
                dcc.Graph(
                    id='graph-all-keywords',
                    figure=all_key_bar,
                ),
                dcc.Graph(
                    id='network-all-keywords',
                    figure=net,
                ),
                dcc.Slider(
                    id='allkeywords-slider',
                    min=0,
                    max=700,
                    step=20,
                    value=400,
                    marks={
                        0: {'label': '0', 'style': {'color': '#77b0b1'}},
                        100: {'label': '100'},
                        200: {'label': '200'},
                        300: {'label': '300'},
                        400: {'label': '400'},
                        500: {'label': '500'},
                        600: {'label': '600'},
                        700: {'label': '700'},
                    }, ),
                html.Div(id='slider-k-output-container', style={'text-align': 'center'}),
                dcc.Slider(
                    id='jaccard-slider',
                    min=0,
                    max=2,
                    step=0.1,
                    value=0.6,
                    marks={
                        0: {'label': '0', 'style': {'color': '#77b0b1'}},
                        1: {'label': '1'},
                        2: {'label': '2'},

                    }),
                html.Div(id='slider-j-output-container', style={'text-align': 'center'}),
                dcc.Checklist(
                    id='drop-checkbox',
                    options=[
                        {'label': ' Drop nodes without connections (and reload)', 'value': 1},
                    ],
                    style={
                        'margin-top': '20px',
                        'text-align': 'center'
                    },
                    value=[1]
                )
            ])], style={
            'left': '10px'
        }
        ),
        dbc.Card([
            dbc.CardHeader('Meta-Data'),
            dbc.CardBody([
                dcc.Graph(
                    id='7-day-mov-average',
                    figure=moving_dates,
                ),
                dcc.Graph(
                    id='authors',
                    figure=author_bar,
                ),
                dcc.Graph(
                    id='categories',
                    figure=category_chart,
                )

            ])], )
    ], style={
        'margin-top': '10px',
    }
    ),

    # Third Block: Single Keyword Section
    dbc.Card([
        dbc.CardHeader('Single Keyword Analysis'),
        dbc.CardBody([
            html.Div([
                dcc.Dropdown(
                    id='SingleKeywordsSelect',
                    options=[
                        {'label': str(keyword), 'value': str(keyword)} for keyword in get_keyword_list(data)
                    ],
                    value='clean energy'
                ),
                html.Div(id='dd-output-container')
            ]),
            html.Div([
                html.Div([
                    dcc.Graph(
                        id='Single-keyword-network',
                        figure=single_network,
                    )], className="six columns"),

                html.Div([
                    dcc.Graph(
                        id='Single-keyword-plot',
                        figure=single_keyword_bar,
                    ),
                ], className="six columns")

            ], className="row"),
            html.Div([
                html.Div([
                    dcc.Graph(
                        id='Single-keyword-time',
                        figure=single_key_time
                    )
                ])
            ]),
            html.Div([
                html.Label('Keywords that correlate highest with selected Keyword:', style={'text-align': 'center'}),
                html.Label(id='correlation-output-container', style={'text-align': 'center'}),
                html.Div(id='google-content', style={'text-align': 'center', 'height': '50px'})
            ])
        ])], style={
        'left': '10px',
        'right': '10px',
        'top': '10px'
    }),
    # Forth Block: Filter Section
    dbc.Card([
        dbc.CardHeader('Filter Section'),
        dbc.CardBody([
            html.Div([
                html.Label("Sort by:"),
                dcc.RadioItems(
                    id='Filter-Select',
                    options=[
                        {'label': ' Keyword Count', 'value': 'Keyword'},
                        {'label': ' Date', 'value': 'Date'}
                    ],
                    value='Keyword',
                    style={"margin": "5px"}
                ),
                html.Label("Filter Options:"),
                dcc.Dropdown(
                    id='TableKeywordsSelect',
                    options=[
                        {'label': str(keyword), 'value': str(keyword)} for keyword in get_keyword_list(data)
                    ],
                    value='clean energy',
                    style={
                        'margin-top': '20px'
                    }
                ),
                dcc.Dropdown(
                    id='TableCategorySelect',
                    options=[
                        {'label': str(categ), 'value': str(categ)} for categ in
                        (np.append(cat['Topic'].unique(), 'all'))
                    ],
                    value='all',
                    style={
                        'margin-top': '20px'
                    }
                ),
                dcc.Dropdown(
                    id='TableDateSelect',
                    options=[
                        {'label': 'Date Newest', 'value': 0},
                        {'label': 'Date Oldest', 'value': 1},
                    ],
                    value=0,
                    style={
                        'margin-top': '20px'
                    }
                ),
            ], className="two columns", style={
                'left': '10px'
            }
            ),

            html.Div([
                dash_table.DataTable(
                    id='table',
                    columns=[{"name": i, "id": i} for i in key_df],
                    data=key_df.to_dict("rows"),
                    style_table={
                        'maxHeight': '80ex',
                        'overflowY': 'scroll',
                        'width': '100%',
                        'minWidth': '100%',
                    },
                    style_cell={
                        'whiteSpace': 'normal',
                        'height': 'auto',
                    },
                )
            ], className="ten columns")

        ]
        ),
    ], style={
        'left': '10px',
        'right': '10px',
        'top': '20px'}
    ),
    dbc.Row(dbc.Col([
        html.Label("A Dash Web Application by Florian Gräbe"),
    ]), style={
        'margin-top': '25px',
        'margin-left': '10px'
    }
    ),
])


# Callbacks
# Callback for All-Keywords Dropdown
@app.callback(
    dash.dependencies.Output('graph-all-keywords', 'figure'),
    [dash.dependencies.Input('allKeywordsDD', 'value')])
def update_all_keywords_graph(value):
    return get_all_keywords_bargraph(data, value)


# Callback for Sliders
@app.callback(
    [dash.dependencies.Output('slider-j-output-container', 'children'),
     dash.dependencies.Output('network-all-keywords', 'figure'),
     dash.dependencies.Output('slider-k-output-container', 'children')],
    [dash.dependencies.Input('allkeywords-slider', 'value'),
     dash.dependencies.Input('jaccard-slider', 'value'),
     dash.dependencies.Input('drop-checkbox', 'value')])
def update_keyword_thr(keyword_thr, jac, drop):
    return 'Min. Jaccard-Koefficient for Edge to be displayed: {}'.format(jac), draw_total_network(data, jac,
                                                                                                   float(keyword_thr),
                                                                                                   drop), \
           'Occurance threshold for keywords (Min.): {}'.format(keyword_thr)


# Callbacks for SingleKeyword Section
@app.callback(
    [dash.dependencies.Output('Single-keyword-network', 'figure'),
     dash.dependencies.Output('Single-keyword-plot', 'figure'),
     dash.dependencies.Output('Single-keyword-time', 'figure'),
     dash.dependencies.Output('correlation-output-container', 'children'),
     dash.dependencies.Output('google-content', 'children')
     ],
    [dash.dependencies.Input('SingleKeywordsSelect', 'value')
     ]
)
def update_single_keywords(value):
    return draw_single_network(data, value), get_single_keyword_bar(data, value), get_keyword_over_time(data,
                                                                                                        value), get_correlated_keywords(
        data, value), get_google_link(value)


# Callbacks for Filter Section
@app.callback(
    [dash.dependencies.Output('table', 'data'),
     dash.dependencies.Output('table', 'columns')],
    [dash.dependencies.Input('TableKeywordsSelect', 'value'),
     dash.dependencies.Input('TableCategorySelect', 'value'),
     dash.dependencies.Input('TableDateSelect', 'value'),
     dash.dependencies.Input('Filter-Select', 'value')]
)
def update_data_table(keyword, category, date, select):
    df = find_articles(data, keyword, category, date, select)
    return df.to_dict("rows"), [{"name": i, "id": i} for i in df]

#function for data loading -> is not working yet -> not compleated since not requested
@app.callback(
    dash.dependencies.Output("modal", "is_open"),
    [dash.dependencies.Input("load-data-button", "n_clicks"),
     dash.dependencies.Input("close", "n_clicks"),
     dash.dependencies.Input("load", "n_clicks")],
    dash.dependencies.Input("data-input", "value"),
    [dash.dependencies.State("modal", "is_open")],
)
def toggle_modal(n1, n2, n3, is_open, value):

    if n1:
        is_open = True

    if n2:
        is_open = False

    if n3:
        data = DataManagement()
        is_open = False

    return is_open


# add callback for toggling the collapse on small screens
@app.callback(
    dash.dependencies.Output("navbar-collapse", "is_open"),
    [dash.dependencies.Input("navbar-toggler", "n_clicks")],
    [dash.dependencies.State("navbar-collapse", "is_open")],
)
def toggle_navbar_collapse(n, is_open):
    if n:
        return not is_open
    return is_open


# Helper Functions
def get_google_link(keyword):
    if " " in keyword:

        link = 'https://trends.google.com/trends/explore?q=' + keyword.split(None, 1)[0] + '%20' + \
               keyword.split(None, 1)[1]
    else:
        link = 'https://trends.google.com/trends/explore?q=' + str(keyword)

    # Build link to Google Trends
    element = html.Div([
        dbc.Row([dbc.Col(html.Label("Visit Google Trends to compare:"))], align="center", ),
        html.A(
            dbc.Row(
                [
                    dbc.Col(html.Label(str(link))),
                ],
                align="center",
            ),
            href=link,
        )
    ])

    return element


if __name__ == '__main__':
    app.run_server(debug=True)
