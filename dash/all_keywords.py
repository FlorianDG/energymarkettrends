import plotly.graph_objs as go
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
pd.options.mode.chained_assignment = None  # default='warn' suppress copy warning


def get_all_keywords_bargraph(data, value):
    total = data.total.copy()
    total = total.T.sort_values(by=0, ascending=False).T

    if value == 1:
        xval = total.T[0][total.T[0] > 100].index
        yval = total.T[0][total.T[0] > 100]
        title = ' Keywords >100 - sorted'
    elif value == 2:
        xval = total.T[0][total.T[0] < 100].index
        yval = total.T[0][total.T[0] < 100]
        title = 'Keywords <100 - sorted'
    else:
        xval = total.columns
        yval = total.loc[0]
        title = 'Occurance of all Keywords - sorted'

    total_data = [go.Bar(
        x=xval,
        y=yval
    )]

    l_keywords = dict(
        title=title
    )

    return dict(data=total_data, layout=l_keywords)


def draw_total_network(data, jac_threshold=0.7, total_thr=100, drop=False):
    total = data.total.copy()
    jaccard = data.jaccard.copy()
    # Draw Graph
    G = nx.Graph()
    # sizes = []
    size = dict()
    # Add Nodes
    for col in total.columns:
        if total.at[0, col] > total_thr:
            G.add_node(col, count=total.at[0, col])
            # sizes.append(total.at[0, col] * 0.02)
            size[col] = float(total.at[0, col] * 0.02)

    # Add edges based on jaccard, but only if the node already exists!
    for keyword in jaccard.columns:
        for col in total.columns:
            # add edge between nodes if jaccard is large enough
            if jaccard.at[keyword, col] > jac_threshold and keyword in G.nodes and col in G.nodes:
                if col != keyword:
                    G.add_edge(keyword, col, weight=jaccard.at[keyword, col])
    if drop:
        G, sizes = drop_single_edges(G, size)
    try:
        edges, weights = zip(*nx.get_edge_attributes(G, 'weight').items())

        options = {
            "edge_color": weights,
            "width": 5,
            "edge_cmap": plt.cm.Blues,
            "with_labels": True,
        }
        nx.draw(G, node_size=list(size.values()), **options)

        # spectral_layout vs spring_layout vs nx.star_graph
        pos_ = nx.spring_layout(G, weight=weights, k=0.4)
    except ValueError:
        print('In Value Error - All Keywords')
        lay_out = go.Layout(title="ERROR - The selected Graph has not enough connections or nodes to be plotted!")
        err = go.Figure(layout=lay_out)
        return err

    # get min for drawing
    wei = [w for w in weights]
    e_min = min(wei)

    # For each edge, make an edge_trace, append to list
    edge_trace = []
    for edge in G.edges():

        if G.edges()[edge]['weight'] > 0:
            char_1 = edge[0]
            char_2 = edge[1]
            x0, y0 = pos_[char_1]
            x1, y1 = pos_[char_2]

            text = char_1 + '--' + char_2 + ': ' + str(G.edges()[edge]['weight'])

            trace = make_edge([x0, x1, None], [y0, y1, None], text,
                              width=1.5 * G.edges()[edge]['weight'], e_min=e_min)
            edge_trace.append(trace)

    # Make a node trace
    node_trace = go.Scatter(x=[],
                            y=[],
                            text=[],
                            textposition="top center",
                            textfont_size=10,
                            mode='markers+text',
                            marker=dict(color='LightSkyBlue',
                                        size=list(size.values()),
                                        line=dict(
                                            color='MediumBlue',
                                            width=1
                                        )))
    # For each node in G, get the position and size and add to the node_trace
    for node in G.nodes():
        x, y = pos_[node]
        node_trace['x'] += tuple([x])
        node_trace['y'] += tuple([y])
        node_trace['text'] += tuple(['<b>' + node + '</b>'])
    # Customize layout
    layout = go.Layout(
        paper_bgcolor='rgba(0,0,0,0)',  # transparent background
        plot_bgcolor='rgba(0,0,0,0)',  # transparent 2nd background
        xaxis={'showgrid': False, 'zeroline': False},  # no gridlines
        yaxis={'showgrid': False, 'zeroline': False},  # no gridlines
    )
    # Create figure
    fig = go.Figure(layout=layout)
    # Add all edge traces
    for trace in edge_trace:
        fig.add_trace(trace)
    # Add node trace
    fig.add_trace(node_trace)
    # Remove legend
    fig.update_layout(showlegend=False)
    # Remove tick labels
    fig.update_xaxes(showticklabels=False)
    fig.update_yaxes(showticklabels=False)

    return fig


### Helper Functions
def get_keywords(data):
    keyw = data.total.copy()
    return list(keyw.T.index)


def sum_columns(dataframe):
    df = pd.DataFrame()
    for column in dataframe.columns:
        df.at[0, column] = dataframe[column].sum()
    return df


def drop_empty_cols(dataframe):
    for colum in dataframe.columns:
        if dataframe.at[0, colum] == 0:
            dataframe = dataframe.drop(columns={colum})
    return dataframe


def sort_keyword_frame(df):
    return df.T.sort_values(by=0, ascending=False).T


def make_edge(x, y, text, width, e_min):
    # TODO: Fix Opacity Change and all related changes/issues
    return go.Scatter(x=x,
                      y=y,
                      # marker_colorscale=plotly.colors.sequential.BuPu,
                      line=dict(width=width,
                                color='cornflowerblue',
                                ),
                      hoverinfo='text',
                      opacity=(1 / (2 * 1.5)) * width,
                      text=([text]),
                      mode='lines')


def drop_single_edges(graph, size):
    remove = [node for node, degree in dict(graph.degree()).items() if degree < 1]
    # remove node from graph
    graph.remove_nodes_from(remove)
    # print(remove)

    # Remove size from sizelist
    for node in remove:
        del size[node]

    return graph, size
