# Python file to display metadata info
import plotly.graph_objs as go
import pandas as pd
import plotly.express as px
pd.options.mode.chained_assignment = None  # default='warn' suppress copy warning


# Code for fig2
def get_moving_avg_date_graph(data):
    daten = data.daten.copy()
    daten.drop(columns={'Unnamed: 0'}, inplace=True)
    daten['Date'] = pd.to_datetime(daten['Date'], format='%b %d %Y')
    dates = daten.groupby('Date').count()
    dates.drop(columns={'URL', 'Teasertext', 'Fulltext', 'Topic', 'DiveBrief', 'Author', 'FullTextWordTokens',
                        'FullTextLemma'}, inplace=True)
    dates = dates.rename(columns={'Title': 'Count'})

    dates['Dates'] = dates.index
    dates_fulldf = pd.DataFrame(columns={'Date', 'Count'})
    dates_fulldf['Date'] = pd.date_range(dates['Dates'].min(), dates['Dates'].max())
    dates['Dates'] = pd.to_datetime(dates['Dates'], format='%b %d %Y')
    dates_fulldf['Date'] = pd.to_datetime(dates_fulldf['Date'], format='%b %d %Y')
    dates_fulldf.set_index('Date', inplace=True, drop=False)
    dates.set_index('Dates', inplace=True, drop=False)

    # loop over dates to build correct dataframe for display#
    dates_fulldf['Count'] = 0
    for d in dates_fulldf['Date']:
        if d in dates['Dates']:
            dates_fulldf.at[d, 'Count'] = dates.at[d, 'Count']
        else:
            # set date to 0 if keyword did not occur here
            dates_fulldf.at[d, 'Count'] = 0

    dates_fulldf['Seven Day Mean'] = dates_fulldf['Count'].rolling(7).mean()
    # drop NaTs
    dates_fulldf = dates_fulldf[~pd.isnull(dates_fulldf['Date'])]
    fig = go.Figure()
    fig.add_trace(go.Scatter(
        x=dates_fulldf['Date'],
        y=dates_fulldf['Seven Day Mean'],
        name="Seven day rolling mean of published articles over time",
        connectgaps=True
    ))
    fig.update_layout(
        title="Seven day rolling mean of published articles over time",
    )

    return fig


def get_category_pie(data):
    daten = data.daten.copy()
    pie_dat = daten
    try:
        pie_dat.drop(columns={'Unnamed: 0'}, inplace=True)
    except Exception:
        print("Cant Drop 'Unnamed: 0'")
        pass

    topics = pie_dat.Topic.unique()
    topics_sum = pd.DataFrame(columns=topics)
    for col in topics_sum.columns:
        topics_sum.at[0, col] = pie_dat.Topic.str.count(col).sum()

    piechart = px.pie(topics_sum,
                      names=topics_sum.columns,
                      values=topics_sum.loc[0],
                      hole=.3,
                      color_discrete_sequence=px.colors.sequential.PuBu,
                      title='Articles per Category'
                      )

    return piechart


def get_author_bar(data):
    authors = data.daten.copy()
    authors = authors.groupby('Author').count()
    authors.drop(
        columns={'URL', 'Teasertext', 'Fulltext', 'Topic', 'DiveBrief', 'Date', 'FullTextWordTokens', 'FullTextLemma'},
        inplace=True)
    authors = authors.rename(columns={'Title': 'Count'})
    authors = authors[authors['Count'] > 10]
    authors.sort_values(by=['Count'], inplace=True, ascending=False)
    authors['Author'] = authors.index
    dat = [go.Bar(
        x=authors['Author'],
        y=authors['Count'])]
    layout = dict(
        title='Number of Articles published by Author (>10)'
    )

    return dict(data=dat, layout=layout)


def get_keyword_list(data):
    keywords = list(data.total.columns)
    return list(keywords)
