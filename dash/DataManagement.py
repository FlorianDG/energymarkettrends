import pandas as pd


###
# This file manages all read and write operations on the data and makes sure that there is only one version of the data
##

class DataManagement:
    def __init__(self):
        self.daten = pd.read_csv('../data/preprocessed/10_02PreProUDText.csv')

        self.all_keywords = pd.read_csv('../data/analysis/texts_allkeywords10_08.csv')
        self.all_keywords.drop(columns={'Unnamed: 0'}, inplace=True)

        self.total = pd.read_csv('../data/analysis/keywords_all10_08.csv')
        self.total.drop(columns={'Unnamed: 0'}, inplace=True)

        self.jaccard = pd.read_csv('../data/preprocessed/10_28jaccard.csv', index_col=0)

        self.urls = pd.read_csv('../data/preprocessed/10_02PreProUDText.csv', sep=',', usecols=['URL', 'Title'],
                                squeeze=True)

        self.dates = pd.read_csv('../data/preprocessed/10_02PreProUDText.csv', sep=',', usecols=['Date', 'Title'],
                                 squeeze=True)
        self.dates['Date'] = pd.to_datetime(self.dates['Date'], format='%b %d %Y')

        self.categories = pd.read_csv('../data/preprocessed/10_02PreProUDText.csv', sep=',', usecols=['Topic', 'Title'],
                                      squeeze=True)
        self.korr = pd.read_csv('../data/preprocessed/correlation.csv', index_col=0)
